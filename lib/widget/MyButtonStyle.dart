import 'package:flutter/material.dart';
import 'package:uno_counter/widget/MyTextStyle.dart';
import 'package:uno_counter/widget/UnoColors.dart';

class MyButtonStyle {
  static get({showBorder = true, borderColor = null}) {
    if (borderColor == null) {
      borderColor = UnoColors.getRandom();
    }
    return ButtonStyle(
      backgroundColor: MaterialStateProperty.all<Color?>(Colors.black),
      textStyle: MaterialStateProperty.all<TextStyle?>(MyTextStyle.get()),
      shape: MaterialStateProperty.all<OutlinedBorder?>(
        RoundedRectangleBorder(
          side: BorderSide(
            color: showBorder ? borderColor : Colors.transparent,
            width: 1,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(50),
        ),
      ),
    );
  }
}

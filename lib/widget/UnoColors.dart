import 'package:flutter/material.dart';
import "dart:math";

class UnoColors {
  static getRed() {
    return Colors.red;
  }

  static getBlue() {
    return Colors.blue;
  }

  static getGreen() {
    return Colors.green;
  }

  static getYellow() {
    return Colors.yellow;
  }

  static getRandom() {
    final _random = new Random();
    var list = [Colors.blue, Colors.red, Colors.yellow, Colors.green];
    return list[_random.nextInt(list.length)];
  }

  static getColorByString(String c) {
    Color res;
    if (c == "Red") {
      res = getRed();
    } else if (c == "Green") {
      res = getGreen();
    } else if (c == "Blue") {
      res = getBlue();
    } else if (c == "Yellow") {
      res = getYellow();
    } else {
      res = Colors.white;
    }

    return res;
  }
}

import 'package:flutter/material.dart';

class MyAppBar {
  static get({bool backButton = false}) {
    return AppBar(
      backgroundColor: Colors.black,
      title: Image.asset("lib/images/uno_logo.png",
          width: AppBar().preferredSize.height),
      centerTitle: true,
      leading: backButton ? BackButton(color: Colors.white) : null,
      automaticallyImplyLeading: backButton,
      bottomOpacity: 1,
    );
  }
}

import 'package:flutter/material.dart';

class MyTextStyle {
  static get({fontSize = 16.0}) {
    return TextStyle(color: Colors.white, fontSize: fontSize);
  }
}

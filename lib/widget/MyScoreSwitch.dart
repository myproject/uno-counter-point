import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:uno_counter/game/gameData.dart';

class MyScoreSwitch {
  static get(Color activeBg) {
    return ToggleSwitch(
      initialLabelIndex:
          GameData.getInstance().getGameScoreMax() == 300 ? 0 : 1,
      totalSwitches: 2,
      labels: ['300', '500'],
      onToggle: (index) {
        int score = 300;
        if (index == 1) {
          score = 500;
        }
        GameData.getInstance().setGameScoreMax(score);
      },
      activeBgColor: [activeBg],
      inactiveBgColor: Colors.grey,
      activeFgColor: Colors.black,
      inactiveFgColor: Colors.black,
    );
  }
}

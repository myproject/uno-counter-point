import 'package:flutter/material.dart';

class MySnackBar {
  static get(context, message, {error = false}) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: error ? Colors.red : Colors.green,
    ));
  }
}

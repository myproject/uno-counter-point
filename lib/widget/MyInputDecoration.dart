import 'package:flutter/material.dart';
import 'package:uno_counter/widget/MyTextStyle.dart';

class MyInputDecoration {
  static get(String hint) {
    return InputDecoration(
        hintText: hint,
        hintStyle: TextStyle(color: Colors.grey),
        border: OutlineInputBorder(),
        enabledBorder:
            OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
        label: Text(hint),
        labelStyle: TextStyle(color: Colors.grey));
  }
}

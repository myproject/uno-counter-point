import 'dart:io';

import 'package:flutter/material.dart';
import 'package:uno_counter/data/Language.dart';
import 'package:uno_counter/game/gameData.dart';
import 'package:uno_counter/view/MyHomePage.dart';
import 'package:uno_counter/websocket/MyWebsocket.dart';
import 'package:uno_counter/widget/MyAppBar.dart';
import 'package:uno_counter/widget/MyButtonStyle.dart';
import 'package:uno_counter/widget/MyInputDecoration.dart';
import 'package:uno_counter/widget/MyScoreSwitch.dart';
import 'package:uno_counter/widget/MySnackBar.dart';
import 'package:uno_counter/widget/MyTextStyle.dart';
import 'package:uno_counter/widget/UnoColors.dart';

class SelectRoomPage extends StatefulWidget {
  @override
  State<SelectRoomPage> createState() => _SelectRoomPage();
}

/**
 * this view is use to select beetwen create party or join party in multi game
 */
class _SelectRoomPage extends State<SelectRoomPage> {
  String _name = "";
  String _code = "";

  /**
   * check if name attribut is correct
   */
  bool isGoodName() {
    bool res = true;
    if (_name.contains("'") ||
        _name.contains('"') ||
        _name.contains("!") ||
        _name.trim() == "") {
      MySnackBar.get(context, Language.getTraductionByKey("incorrectName"),
          error: true);
      res = false;
    }

    return res;
  }

  @override
  Widget build(BuildContext context) {
    websocket.setContext(context);

    return Scaffold(
      appBar: MyAppBar.get(backButton: true),
      backgroundColor: Colors.black,
      body: WillPopScope(
        onWillPop: () async {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => MyHomePage()));
          return false;
        },
        child: SingleChildScrollView(
          child: Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.8,
              child: Column(
                children: [
                  TextFormField(
                    style: MyTextStyle.get(),
                    decoration: MyInputDecoration.get(
                        Language.getTraductionByKey("yourNameHint")),
                    maxLength: 30,
                    onChanged: (value) {
                      _name = value as String;
                    },
                  ),
                  Row(
                    children: [
                      MyScoreSwitch.get(UnoColors.getRandom()),
                      ElevatedButton(
                        onPressed: () {
                          if (isGoodName()) {
                            websocket.send('{"command": "setname", "name": "' +
                                _name +
                                '"}');
                            websocket.send(
                                '{"command": "create", "scoreMax": "' +
                                    GameData.getInstance()
                                        .getGameScoreMax()
                                        .toString() +
                                    '"}');
                          }
                        },
                        child: Text(
                            Language.getTraductionByKey("createPartyButton")),
                        style: MyButtonStyle.get(),
                      ),
                    ],
                  ),
                  TextFormField(
                    style: MyTextStyle.get(),
                    decoration: MyInputDecoration.get(
                        Language.getTraductionByKey("partiCodeHint")),
                    maxLength: 4,
                    onChanged: (value) {
                      _code = value as String;
                    },
                  ),
                  ElevatedButton(
                    onPressed: () {
                      if (isGoodName()) {
                        if (_code.length == 4 && _code.trim() != "") {
                          websocket.send('{"command": "setname", "name": "' +
                              _name +
                              '"}');
                          websocket.send(
                              '{"command": "join", "id": "' + _code + '"}');
                          GameData.getInstance().setPartyCode(_code);
                        } else {
                          MySnackBar.get(context,
                              Language.getTraductionByKey("incorrectCode"),
                              error: true);
                        }
                      }
                    },
                    child: Text(Language.getTraductionByKey("joinPartyButton")),
                    style: MyButtonStyle.get(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:uno_counter/data/CardColor.dart';
import 'package:uno_counter/data/Language.dart';
import 'package:uno_counter/game/Player.dart';
import 'package:uno_counter/game/gameData.dart';
import 'package:uno_counter/view/AllPlayerScore.dart';
import 'package:uno_counter/websocket/MyWebsocket.dart';
import 'package:uno_counter/widget/MyAppBar.dart';
import 'package:uno_counter/widget/MyButtonStyle.dart';
import 'package:uno_counter/widget/MyTextStyle.dart';
import 'package:uno_counter/widget/UnoColors.dart';

class AddScorePage extends StatefulWidget {
  @override
  State<AddScorePage> createState() => _AddScorePage();
}

/**
 * this view is use to add score to player
 */
class _AddScorePage extends State<AddScorePage> {
  final List<List<String>> _allCard = [
    ["0", "1", "2", "3", "4"],
    ["5", "6", "7", "8", "9"],
    ["passTurn", "add2", "changeSens"],
    ["changeColor", "add4"]
  ];
  final Map _scoreCard = {
    "0": 0,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "passTurn": 20,
    "add2": 20,
    "changeSens": 20,
    "changeColor": 50,
    "add4": 50
  };
  int _score = 0;
  bool _enabledConfirmButton = true;
  Color addScoreButtonColor = UnoColors.getRandom();
  Color settingButtonColor = UnoColors.getRandom();
  Map allCardColor = {};
  List<String> selectedCards = [];

  void setCardColor() {
    for (List<String> cardLine in _allCard) {
      for (String card in cardLine) {
        allCardColor[card] = CardColor.getSelectedColor() + "Card" + card;
      }
    }
  }

  void initState() {
    super.initState();
    setCardColor();
  }

  @override
  Widget build(BuildContext context) {
    websocket.setContext(context);

    return Scaffold(
      floatingActionButton: SpeedDial(
        backgroundColor: Colors.black,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: settingButtonColor,
            width: 1,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(50),
        ),
        animatedIcon: AnimatedIcons.menu_close,
        overlayOpacity: 0,
        children: [
          for (String color in CardColor.getAvailableColor())
            SpeedDialChild(
                label: color,
                child: Text(color[0]),
                backgroundColor: UnoColors.getColorByString(color),
                labelBackgroundColor: UnoColors.getColorByString(color),
                onTap: () {
                  CardColor.setSelectedColor(color);
                  setState(() {
                    setCardColor();
                  });
                }),
        ],
      ),
      appBar: MyAppBar.get(backButton: true),
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Text(
                Language.getTraductionByKey("scoreAddLabel") +
                    " " +
                    _score.toString(),
                style: MyTextStyle.get(),
              ),
              Text(
                Language.getTraductionByKey("scoreTotalLabel") +
                    " " +
                    (GameData.getInstance().getCurrentPlayer().getScore() +
                            _score)
                        .toString(),
                style: MyTextStyle.get(),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    for (String card in selectedCards)
                      Container(
                        width: MediaQuery.of(context).size.width * 0.15,
                        child: Image.asset(
                            "lib/images/" + allCardColor[card] + ".png"),
                        padding: EdgeInsets.only(left: 10, top: 10),
                      ),
                  ],
                ),
              ),
              for (List<String> cardLine in _allCard)
                Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Row(
                    children: [
                      for (String card in cardLine)
                        Container(
                          width: MediaQuery.of(context).size.width * 0.2,
                          child: ElevatedButton(
                            onPressed: () {
                              setState(() {
                                _score += _scoreCard[card] as int;
                                selectedCards.add(card);
                              });
                            },
                            style: MyButtonStyle.get(showBorder: false),
                            child: Image.asset(
                                "lib/images/" + allCardColor[card] + ".png"),
                          ),
                        ),
                    ],
                  ),
                ),
              Padding(padding: EdgeInsets.only(top: 30)),
              if (_enabledConfirmButton)
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _enabledConfirmButton = false;
                        if (GameData.getInstance().getGameType() == "multi") {
                          websocket.send('{"command": "addscore", "score": "' +
                              _score.toString() +
                              '"}');
                        } else {
                          Player p = GameData.getInstance().getCurrentPlayer();
                          GameData.getInstance().setScoreToPlayer(
                              p.getName(), p.getScore() + _score);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AllPlayerScore()));
                        }
                      });
                    },
                    style: MyButtonStyle.get(borderColor: addScoreButtonColor),
                    child: Text(Language.getTraductionByKey("addPointButton")))
            ],
          ),
        ),
      ),
    );
  }
}

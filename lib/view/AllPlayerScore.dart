import 'dart:async';
import 'package:flutter/material.dart';
import 'package:uno_counter/data/Language.dart';
import 'package:uno_counter/game/Player.dart';
import 'package:uno_counter/game/gameData.dart';
import 'package:uno_counter/view/AddScorePage.dart';
import 'package:uno_counter/view/MyHomePage.dart';
import 'package:uno_counter/websocket/MyWebsocket.dart';
import 'package:uno_counter/widget/MyAppBar.dart';
import 'package:uno_counter/widget/MyButtonStyle.dart';
import 'package:uno_counter/widget/MyInputDecoration.dart';
import 'package:uno_counter/widget/MyScoreSwitch.dart';
import 'package:uno_counter/widget/MySnackBar.dart';
import 'package:uno_counter/widget/MyTextStyle.dart';
import 'package:uno_counter/widget/UnoColors.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:uno_counter/data/settingsDao.dart';

class AllPlayerScore extends StatefulWidget {
  @override
  State<AllPlayerScore> createState() => _AllPlayerScore();
}

/**
 * this view is use to see score of each player in a game
 */
class _AllPlayerScore extends State<AllPlayerScore> {
  //all game type attribute
  List<Player> _players = GameData.getInstance().getPlayers();
  late Timer _refreshTask;
  final Color _leavePartyButtonColor = UnoColors.getRandom();
  final Color _switchScoreColor = UnoColors.getRandom();

  //singleGameType attribute
  String _playerName = "";
  final Color _addPlayerButtonColor = UnoColors.getRandom();

  //for the moment only single maybe multi in another version
  final Color settingButtonColor = UnoColors.getRandom();
  String order = "";
  
  void orderPlayer() {
    setState(() {
          _players = GameData.getInstance().getPlayers();
          if(order == "croissant") {
            _players.sort((a, b) => a.getScore().compareTo(b.getScore()));
          } else {
            _players.sort((a, b) => -a.getScore().compareTo(b.getScore()));
          }
        });
  }

  void initState() {
    super.initState();
    DaoSettings().getSettings().then((value) {
      print(value["order"]);
        setState(() {
          if(value["order"] == null) {
          DaoSettings().insertSettings({"key": "order", "value": "croissant"});
          order = "croissant";
        } else {
          order = value["order"] as String;
        }

        orderPlayer();
      });
    });

    if (GameData.getInstance().getGameType() == "multi") {
      _refreshTask = Timer.periodic(Duration(seconds: 1), (timer) {
        orderPlayer();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    websocket.setContext(context);

    return Scaffold(
      floatingActionButton: SpeedDial(
        backgroundColor: Colors.black,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: settingButtonColor,
            width: 1,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(50),
        ),
        animatedIcon: AnimatedIcons.menu_close,
        overlayOpacity: 0,
        children: [
          SpeedDialChild(
            label: Language.getTraductionByKey("leavePartyButton"),
            child: Icon(Icons.cancel_outlined, color: Colors.red, size: 45,),
            backgroundColor: Colors.white,
            labelBackgroundColor: Colors.white,
            onTap: () async {
              if (GameData.getInstance().getGameType() == "multi") {
                websocket.send('{"command": "leave"}');
                _refreshTask.cancel();
              } else {
                GameData.getInstance().resetParty();
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MyHomePage()));
              }
            }
          ),
          if(GameData.getInstance().getGameScoreMax() == 300)
          SpeedDialChild(
            label: Language.getTraductionByKey("changeScoreTo") + " 500",
            child: Text("500"),
            backgroundColor: Colors.white,
            labelBackgroundColor: Colors.white,
            onTap: () {
              if(GameData.getInstance().getGameType() == "multi") {
                websocket.send('{"command": "setscoreparty","score":"500"}');
              }
              setState(() {
                GameData.getInstance().setGameScoreMax(500);
              });
            }
          ),
          if(GameData.getInstance().getGameScoreMax() == 500)
          SpeedDialChild(
            label: Language.getTraductionByKey("changeScoreTo") + " 300",
            child: Text("300"),
            backgroundColor: Colors.white,
            labelBackgroundColor: Colors.white,
            onTap: () {
              if(GameData.getInstance().getGameType() == "multi") {
                websocket.send('{"command": "setscoreparty","score":"300"}');
              }
              setState(() {
                GameData.getInstance().setGameScoreMax(300);
              });
            }
          ),
          if(order == "decroissant")
          SpeedDialChild(
            label: Language.getTraductionByKey("crescent"),
            child: Text("0 > 1"),
            backgroundColor: Colors.white,
            labelBackgroundColor: Colors.white,
            onTap: () async {
              await DaoSettings().updateSettings("order", "croissant");
              order = "croissant";
              orderPlayer();
            }
          ),

          if(order == "croissant")
          SpeedDialChild(
            label: Language.getTraductionByKey("decreasing"),
            child: Text("1 > 0"),
            backgroundColor: Colors.white,
            labelBackgroundColor: Colors.white,
            onTap: () async {
              await DaoSettings().updateSettings("order", "decroissant");
              order = "decroissant";
              orderPlayer();
            }
          ),
        ],
      ),
      appBar: MyAppBar.get(),
      backgroundColor: Colors.black,
      body: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                if (GameData.getInstance().getGameType() == "multi")
                  Container(
                    margin: EdgeInsets.only(bottom: 5),
                    child: Text(
                      Language.getTraductionByKey("codeRoom") +
                          ": " +
                          GameData.getInstance().getPartyCode(),
                      style: MyTextStyle.get(),
                    ),
                  ),
                Container(
                  margin: EdgeInsets.only(bottom: 50),
                  child: Text(
                    Language.getTraductionByKey("scoreForLoose") +
                        ": " +
                        GameData.getInstance().getGameScoreMax().toString(),
                    style: MyTextStyle.get(),
                  ),
                ),
                if (GameData.getInstance().getGameType() == "single" &&
                    !GameData.getInstance().gameHasStart())
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: TextFormField(
                            onChanged: (value) {
                              _playerName = value;
                            },
                            style: MyTextStyle.get(),
                            decoration: MyInputDecoration.get(
                                Language.getTraductionByKey("playerNameHint")),
                          ),
                          width: MediaQuery.of(context).size.width * 0.3,
                          margin: EdgeInsets.only(
                              right: MediaQuery.of(context).size.width * 0.1),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 50),
                          width: MediaQuery.of(context).size.width * 0.5,
                          child: ElevatedButton(
                            style: MyButtonStyle.get(
                                borderColor: _addPlayerButtonColor),
                            onPressed: () {
                              if (_playerName.trim() != "") {
                                Player p = Player(_playerName, 0);
                                if (GameData.getInstance()
                                    .playerAreInParty(p)) {
                                  MySnackBar.get(
                                      context, "Player already in party",
                                      error: true);
                                } else {
                                  GameData.getInstance().addPlayer(p);
                                  setState(() {
                                    _players =
                                        GameData.getInstance().getPlayers();
                                  });
                                }
                              }
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(Language.getTraductionByKey(
                                    "addPlayerButton")),
                                Icon(
                                  Icons.add,
                                  color: Colors.white,
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                if (GameData.getInstance().getGameType() == "single")
                  Padding(padding: EdgeInsets.only(top: 50)),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  width: MediaQuery.of(context).size.width * 0.6,
                  child: Row(
                    children: [
                      Padding(padding: EdgeInsets.only(left: 20)),
                      Text(
                        Language.getTraductionByKey("nameLabel"),
                        style: MyTextStyle.get(fontSize: 20.0),
                      ),
                      Spacer(),
                      Text(
                        Language.getTraductionByKey("scoreLabel"),
                        style: MyTextStyle.get(fontSize: 20.0),
                      ),
                      Padding(padding: EdgeInsets.only(left: 20))
                    ],
                  ),
                ),
                for (Player player in _players)
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: player.hasLoose() ? Colors.red : Colors.black),
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Row(
                      children: [
                        if(!GameData.getInstance().gameHasStart() && GameData.getInstance().getGameType() == "single")
                        IconButton(
                          onPressed: () {
                            GameData.getInstance().removePlayer(player.getName());
                            setState(() {});
                          }, 
                          icon: Icon(
                            Icons.remove_circle_outline, 
                            color: Colors.red,
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(left: 20)),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.29,
                          child: Text(
                            player.getName() + ":",
                            style: MyTextStyle.get(fontSize: 19.0),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Spacer(),
                        Text(
                          player.getScore().toString(),
                          style: MyTextStyle.get(fontSize: 19.0),
                        ),
                        Padding(padding: EdgeInsets.only(left: 20)),
                        if ((GameData.getInstance().getCurrentPlayer() ==
                                    player ||
                                GameData.getInstance().getGameType() ==
                                    "single") &&
                            player.getScore() <
                                GameData.getInstance().getGameScoreMax() &&
                            !GameData.getInstance().gameContainLooser() &&
                            _players.length >= 2)
                          IconButton(
                              onPressed: () {
                                if (GameData.getInstance().getGameType() ==
                                    "single") {
                                  GameData.getInstance()
                                      .setCurrentPlayer(player);
                                }
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => AddScorePage()));
                              },
                              splashColor: Colors.transparent,
                              icon: Icon(
                                Icons.add,
                                color: Colors.white,
                              ),
                            )
                      ],
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uno_counter/data/Language.dart';
import 'package:uno_counter/game/gameData.dart';
import 'package:uno_counter/view/AllPlayerScore.dart';
import 'package:uno_counter/view/manyPhone/SelectRoomPage.dart';
import 'package:uno_counter/widget/MyAppBar.dart';
import 'package:uno_counter/widget/MyButtonStyle.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:uno_counter/widget/UnoColors.dart';

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

/**
 * this view is the main page (select beetwen 1 phone or multi phone)
 */
class _MyHomePageState extends State<MyHomePage> {
  final onePhoneButton = UnoColors.getRandom();
  final manyPhoneButton = UnoColors.getRandom();
  final leaveButton = UnoColors.getRandom();
  final settingsButton = UnoColors.getRandom();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: SpeedDial(
        backgroundColor: Colors.black,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: settingsButton,
            width: 1,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(50),
        ),
        animatedIcon: AnimatedIcons.menu_close,
        overlayOpacity: 0,
        children: [
          for (List<String> language in Language.getAvailableLanguage())
            SpeedDialChild(
                label: language[1],
                child: Text(language[0]),
                onTap: () {
                  Language.setSelectedLanguage(language[0]);
                  setState(() {});
                }),
        ],
      ),
      appBar: MyAppBar.get(),
      backgroundColor: Colors.black,
      body: Center(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.20),
              child: Column(
                children: [
                  ElevatedButton(
                    onPressed: () {
                      GameData.getInstance().setGameType("single");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AllPlayerScore()));
                    },
                    child: Text(Language.getTraductionByKey("onePhone")),
                    style: MyButtonStyle.get(borderColor: onePhoneButton),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.05),
                    child: ElevatedButton(
                      onPressed: () {
                        GameData.getInstance().setGameType("multi");
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SelectRoomPage()));
                      },
                      child: Text(Language.getTraductionByKey("manyTelephone")),
                      style: MyButtonStyle.get(borderColor: manyPhoneButton),
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            Container(
              padding: EdgeInsets.only(bottom: 20),
              child: ElevatedButton(
                onPressed: () {
                  SystemNavigator.pop();
                },
                child: Text(Language.getTraductionByKey("leave")),
                style: MyButtonStyle.get(borderColor: leaveButton),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

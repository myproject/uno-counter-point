import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:uno_counter/data/Language.dart';
import 'package:uno_counter/view/MyHomePage.dart';
import 'package:uno_counter/widget/UnoColors.dart';

class LaunchLoadPage extends StatefulWidget {
  @override
  State<LaunchLoadPage> createState() => _LaunchLoadPage();
}

/**
 * this view is use to see score of each player in a game
 */
class _LaunchLoadPage extends State<LaunchLoadPage> {
  void loadLanguage(BuildContext context) async {
    await Language.init();
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MyHomePage()));
  }

  @override
  Widget build(BuildContext context) {
    loadLanguage(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child:
            LoadingBouncingLine.circle(backgroundColor: UnoColors.getRandom()),
      ),
    );
  }
}

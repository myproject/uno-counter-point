import 'package:flutter/material.dart';
import 'package:uno_counter/data/Language.dart';
import 'package:uno_counter/game/Player.dart';
import 'package:uno_counter/game/gameData.dart';
import 'package:uno_counter/view/manyPhone/SelectRoomPage.dart';
import 'package:uno_counter/view/allPlayerScore.dart';
import 'package:uno_counter/websocket/MyWebsocket.dart';
import 'package:uno_counter/widget/MySnackBar.dart';

class Commands {
  /**
   * check if command has fail or not
   */
  static hasFailed(dynamic args) {
    bool isFail;

    if (args["state"] == "success") {
      isFail = false;
    } else {
      isFail = true;
    }

    return isFail;
  }

  /**
   * display the command message in snackBar
   */
  static displayMessage(BuildContext context, dynamic args) {
    MySnackBar.get(context, Language.getTraductionByKey(args["messageCode"]),
        error: hasFailed(args));
  }

  /**
   * set name command
   */
  static setNameCommands(BuildContext context, dynamic args) {
    displayMessage(context, args);
    GameData.getInstance().setCurrentPlayer(Player(args["args"][0], 0));
  }

  /**
   * create command
   */
  static createCommands(BuildContext context, dynamic args) {
    displayMessage(context, args);

    if (!hasFailed(args)) {
      GameData.getInstance().addCurrentPlayerToPlayerInGame();
      GameData.getInstance().setPartyCode(args["args"][0]);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => AllPlayerScore()));
    }
  }

  /**
   * leave commands
   */
  static leaveCommands(BuildContext context, dynamic args) {
    displayMessage(context, args);

    if (!hasFailed(args)) {
      GameData.getInstance().resetParty();
      GameData.getInstance().setGameType("multi");
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => SelectRoomPage()));
    }
  }

  /**
   * join command
   */
  static joinCommands(BuildContext context, dynamic args) {
    displayMessage(context, args);

    if (!hasFailed(args)) {
      GameData.getInstance().setGameScoreMax(int.parse(args["args"][0]));
      websocket.send('{"command": "getscore"}');
      GameData.getInstance().addCurrentPlayerToPlayerInGame();
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => AllPlayerScore()));
    }
  }

  /**
   * get score command
   */
  static getScoreCommands(BuildContext context, dynamic args) {
    displayMessage(context, args);

    if (!hasFailed(args)) {
      var joueurs = args["args"][0]["score"];

      for (var joueur in joueurs) {
        GameData.getInstance()
            .addPlayer(Player(joueur["name"], joueur["score"]));
      }
    }
  }

  /**
   * event "command"
   */
  static event(BuildContext context, dynamic args) {
    switch (args["state"]) {
      case "join":
        {
          GameData.getInstance().addPlayer(Player(args["args"][0], 0));
        }
        break;

      case "leave":
        {
          GameData.getInstance().removePlayer(args["args"][0]);
        }
        break;
      case "endGame":
      case "score":
        {
          GameData.getInstance()
              .setScoreToPlayer(args["args"][0], int.parse(args["args"][1]));
          if (GameData.getInstance().getCurrentPlayer().getName() ==
              args["args"][0]) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AllPlayerScore()));
          }
        }
        break;
      case "setscoreparty":
        {
          GameData.getInstance().setGameScoreMax(int.parse(args["args"][0]));
          Navigator.push(context,
                MaterialPageRoute(builder: (context) => AllPlayerScore()));
        }
        break;

      default:
        {
          print("unknown error");
        }
        break;
    }
  }
}

import 'package:uno_counter/websocket/commands.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
//import 'package:web_socket_channel/io.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

class websocket {
  static final websocket _websocket = websocket();

  /**
   * setter on attribute _context
   */
  static setContext(BuildContext context) {
    _websocket._context = context;
  }

  /**
   * send message to websocket server
   */
  static send(String message) {
    _websocket.channel.sink.add(message);
  }

  final WebSocketChannel channel =
      WebSocketChannel.connect(Uri.parse("ws://10.0.2.2:8081"));
  //WebSocketChannel.connect("ws://k-gouzien.fr:8081");
  late BuildContext _context;

  /**
   * constructor of websocket class
   */
  websocket() {
    channel.stream.listen(
      (data) {
        commandeSplitter(jsonDecode(data));
      },
      onError: (error) => print(error),
    );
  }

  /**
   * check args and split to each command
   */
  void commandeSplitter(dynamic args) {
    switch (args["command"]) {
      case "setname":
        {
          Commands.setNameCommands(_context, args);
        }
        break;

      case "create":
        {
          Commands.createCommands(_context, args);
        }
        break;
      case "event":
        {
          Commands.event(_context, args);
        }
        break;
      case "leave":
        {
          Commands.leaveCommands(_context, args);
        }
        break;
      case "join":
        {
          Commands.joinCommands(_context, args);
        }
        break;
      case "getScore":
        {
          Commands.getScoreCommands(_context, args);
        }
        break;

      default:
        {
          print("unknown error");
        }
        break;
    }
  }
}

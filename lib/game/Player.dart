import 'package:uno_counter/game/gameData.dart';

class Player {
  late String _name;
  late int _score;

  /**
   * constructor of Player class
   */
  Player(String name, int score) {
    _name = name;
    _score = score;
  }

  /**
   * Setter on _score attribut
   */
  void setScore(int score) {
    _score = score;
  }

  /**
   * Getter on _score attribut
   */
  int getScore() {
    return _score;
  }

  /**
   * Getter on _name attribut
   */
  String getName() {
    return _name;
  }

  /**
   * check if Player has equal or more than 500 score
   */
  bool hasLoose() {
    return _score >= GameData.getInstance().getGameScoreMax();
  }
}

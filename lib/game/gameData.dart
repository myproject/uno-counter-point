import 'package:uno_counter/game/Player.dart';

class GameData {
  static GameData _instance = GameData();

  /**
   * getter for the singleton instance of GameData
   */
  static getInstance() {
    return _instance;
  }

  //attribut for all game type
  late String gameType;
  late List<Player> players;
  late Player
      currentPlayer; //multi: current player on phone           single: use for current player whos score is changing
  late int gameScoreMax;

  //single game type attribute
  late bool gameIsStart;

  //multi game type attribute
  late String partyCode;

  /**
   * constructor for GameData class
   */
  GameData() {
    players = [];
    partyCode = "";
    currentPlayer = Player("", 0);
    gameType = "";
    gameIsStart = false;
    gameScoreMax = 300;
  }

  //function for all game type

  /**
   * setter for game score max
   */
  void setGameScoreMax(int scoreMax) {
    gameScoreMax = scoreMax;
  }

  /**
   * getter for game score max
   */
  int getGameScoreMax() {
    return gameScoreMax;
  }

  /**
   * setter for the game type
   */
  void setGameType(String typeOfGame) {
    gameType = typeOfGame;
  }

  /**
   * getter for the game type
   */
  String getGameType() {
    return gameType;
  }

  /**
   *  reset the game data instance
   */
  void resetParty() {
    _instance = GameData();
  }

  /**
   * add a player to the list of players
   */
  void addPlayer(Player player) {
    if (playerAreInParty(player)) {
      setScoreToPlayer(player.getName(), player.getScore());
    } else {
      players.add(player);
    }
  }

  /**
   * check if player are already in game
   */
  bool playerAreInParty(Player player) {
    bool isIn = false;

    for (Player p in players) {
      if (p.getName() == player.getName()) {
        isIn = true;
        break;
      }
    }

    return isIn;
  }

  /**
   * setter on currentPlayer attribut
   */
  void setCurrentPlayer(Player player) {
    currentPlayer = player;
  }

  /**
   * getter on currentPlayer attribut
   */
  Player getCurrentPlayer() {
    return currentPlayer;
  }

  /**
   * set score to a player by name
   */
  void setScoreToPlayer(String playerName, int score) {
    for (int i = 0; i < players.length; ++i) {
      if (playerName == players[i].getName()) {
        players[i].setScore(score);
        break;
      }
    }

    if (gameType == "single") {
      gameIsStart = true;
    }
  }

  /**
   * getter on attribut Players
   */
  List<Player> getPlayers() {
    return players;
  }

  /**
   * return if someone has loose in the game
   */
  bool gameContainLooser() {
    bool hasLooser = false;
    for (Player p in players) {
      if (p.hasLoose()) {
        hasLooser = true;
      }
    }
    return hasLooser;
  }

  //SinglePhoneGame function
  /**
   * getter on gameIsStart attribut
   */
  bool gameHasStart() {
    return gameIsStart;
  }

  //MultiPhoneGame function

  /**
   * add currentPlayer attribut in player list
   */
  void addCurrentPlayerToPlayerInGame() {
    addPlayer(currentPlayer);
  }

  /**
   * setter on partyCode attribut
   */
  void setPartyCode(String code) {
    partyCode = code;
  }

  /**
   * getter on partyCode attribut
   */
  String getPartyCode() {
    return partyCode;
  }

  /**
   * remove a player to the list of players
   */
  void removePlayer(String playerName) {
    for (int i = 0; i < players.length; ++i) {
      if (playerName == players[i].getName()) {
        players.removeAt(i);
        break;
      }
    }
  }
}

import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'dart:io';

class DaoSettings {
  static final _databaseName = "app.db";
  static final _databaseVersion = 1;

  DaoSettings();

  static Database? _database;
  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  static final table = 'settings';
  static final coulumnKey = "key";
  static final columnValue = "value";

  Future _onCreate(Database db, int version) async {
    _createTableSettings(db, version);
  }

  Future<int> getMaxId(String table) async {
    Database db = await DaoSettings().database;
    var res = await db.rawQuery("select max(id) from $table");
    int max;
    if (res[0]["max(id)"] == null) {
      max = 0;
    } else {
      max = int.parse(res[0]["max(id)"].toString());
    }
    return max;
  }

  Future _createTableSettings(Database db, int version) async {
    await db.execute('''
          CREATE TABLE settings (
            id INTEGER AUTO_INCREMENT PRIMARY KEY,
            key TEXT NOT NULL,
            value TEXT NOT NULL
          )
          ''');
  }

  Future resetTableSettings() async {
    Database db = await DaoSettings().database;
    db.rawDelete("DROP TABLE IF EXISTS settings");
    _createTableSettings(db, _databaseVersion);
  }

  Future<Map<String, String>> getSettings() async {
    Database db = await DaoSettings().database;
    List<Map<String, Object?>> rows = await db.query(table);

    Map<String, String> res = {};
    for (Map<String, Object?> row in rows) {
      res[row["key"].toString()] = row["value"].toString();
    }

    return res;
  }

  Future<int> insertSettings(Map<String, dynamic> row) async {
    Database db = await DaoSettings().database;
    var maxId = await getMaxId(table);
    row["id"] = maxId + 1;
    return await db.insert(table, row);
  }

  Future<void> updateSettings(String key, String newValue) async {
    Database db = await DaoSettings().database;
    db.execute('update ' + table + ' set value = "' + newValue + '" where key = "' + key + '"');
  }

}

import 'package:flutter/services.dart';
import 'dart:convert';

class Language {
  static late final dynamic _allLanguague;
  static dynamic _selectedLanguage = {};
  static final List<List<String>> _availableLanguage = [];

  static init() async {
    dynamic value = await _readJsonLanguague();
    _allLanguague = value["lang"];

    for (dynamic language in _allLanguague) {
      _availableLanguage.add([language["lang"], language["fullLang"]]);
    }

    setSelectedLanguage("fr");
  }

  static Future<dynamic> _readJsonLanguague() async {
    final String response = await rootBundle.loadString('lib/language.json');
    return await json.decode(response);
  }

  static List<List<String>> getAvailableLanguage() {
    return _availableLanguage;
  }

  static void setSelectedLanguage(String lang) {
    for (dynamic language in _allLanguague) {
      if (language["lang"] == lang) {
        _selectedLanguage = language;
        break;
      }
    }
  }

  static String getTraductionByKey(String key) {
    return _selectedLanguage[key];
  }
}

import "dart:math";

class CardColor {
  static String _color = "Red";
  static List<String> _availableColor = [
    "Red",
    "Yellow",
    "Green",
    "Blue",
    "Random"
  ];

  static List<String> getAvailableColor() {
    return _availableColor;
  }

  static void setSelectedColor(String color) {
    _color = color;
  }

  static String getSelectedColor() {
    String res = _color;
    if (res == "Random") {
      final _random = new Random();
      while (res == "Random") {
        res = _availableColor[_random.nextInt(_availableColor.length)];
      }
    }

    return res;
  }
}

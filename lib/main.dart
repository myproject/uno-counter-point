import 'package:flutter/material.dart';
import 'package:uno_counter/view/LauchLoadPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Uno Counter',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: LaunchLoadPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
